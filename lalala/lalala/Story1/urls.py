from django.urls import path
from . import views

urlpatterns = [
    path("", views.testy, name="testy"),
    path("gallery/", views.gallery, name="gallery"),
    path("lalala/", views.lalala, name="lalala"),
    path("ohoho/", views.ohoho, name="ohoho")
]
