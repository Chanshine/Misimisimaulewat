from django.http import HttpResponse
from django.shortcuts import render, redirect

def testy(request):
    return render(request, "testy.html")

def gallery(request):
    return render(request, "gallery.html")

def lalala(request):
    return render(request, "lalala.html")

def ohoho(request):
    return render(request, "ohoho.html")